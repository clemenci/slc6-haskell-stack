FROM cern/slc6-base:20170301

RUN yum install -y perl make automake gcc gmp-devel libffi zlib xz tar git gnupg \
 && yum clean all \
 && rm -rf /var/lib/apt/lists/* /lib/modules/* /lib/firmware/* /lib/kbd
RUN curl -sSL https://get.haskellstack.org/ | sh
RUN stack setup
